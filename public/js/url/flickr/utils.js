define([], function() {

	var UTILS = {
		
		/**
		 * Truncate utility method used to truncate provided string based on provided number of letters.
		 * @param  {String} stringToTruncate string to be truncated
		 * @param  {Number} numberOfLetters  number of letters that new string will take from provided string
		 * @param  {String} suffix           suffix to be appended to the truncated string (if not provided defautls to '')
		 * @return {String}                  truncated string
		 */
		truncate: function(stringToTruncate, numberOfLetters, suffix) {
			return stringToTruncate && stringToTruncate.length < numberOfLetters ? stringToTruncate : stringToTruncate.substring(0, numberOfLetters) + suffix || '';
		}

	};

	return UTILS;
});