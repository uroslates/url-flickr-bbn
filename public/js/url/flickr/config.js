define([], function() {

	var CONFIGURATION = {
		
		// Default search fitler to be used for Flicker Service API call
		filter: {
			tags: 'potato'
		}

		, titleLettersLimit: 16

		// Default date format used within the application
		, dateFormat: 'Do MMM YYYY [at] h:mm a'

	};

	return CONFIGURATION;
});