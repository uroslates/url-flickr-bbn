define([
	'backbone'
	, '../utils'
	, '../config'
],
function(
	Backbone
	, utils
	, config
) {

	var ItemModel = Backbone.Model.extend({

		parse: function(response) {
			// Expose extra properties used within the application
			response.published_formatted = moment( response.published ).format(config.dateFormat);
			response.title_trunc = utils.truncate( response.title, config.titleLettersLimit, '...');
			response.fileName = this.getLastSegment(response.media.m);
			response.tagsArray = response.tags.split(' ');

			return response;
		}

		, getFileName: function() {
			var fileName = '';
			if (this.get('media') && this.get('media')) {
				fileName = this.getLastSegment(this.get('media').m);
			}
			return fileName;
		}

		, getLastSegment: function(url) {
			return url.split('/').pop();
		}

	});

	return ItemModel;
});