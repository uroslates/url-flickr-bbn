define([
	'backbone'
	, 'urltemplating'
],
function(
	Backbone
	, TemplatingEngine
) {

	var BaseView = Backbone.View.extend({
		
		serialize: function() {
			var context = {};
			if (this.options) {
				if (this.options.model) {
					context = this.options.model.toJSON();
				}
			}
			return context;
		}

		, render: function() {
			if (!this.template) {
				throw Error('BaseView.render(): <template> property is required!');
			}
			this.$el.html(TemplatingEngine.render(this.template, this.serialize()));
			return this;
		}

	});

	return BaseView;
});