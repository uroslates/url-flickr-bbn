define([
	'./BaseView'
	, 'urltemplating'
	, 'text!../templates/no_items.html'
],
function(
	BaseView
	, TemplatingEngine
	, NoItemsTpl
) {

	var NoItemsView = BaseView.extend({

		template: NoItemsTpl

		, initialize: function() {
			this.options.model = new Backbone.Model({
				message: this.options && this.options.message ? this.options.message : ''
			});
		}


	});

	return NoItemsView;
});