define([
	'./BaseView'
	, 'urltemplating'
	, 'moment'
	, '../utils'
	, 'text!../templates/item.html'
],
function(
	BaseView
	, TemplatingEngine
	, moment
	, utils
	, ItemTpl
) {

	var ItemView = BaseView.extend({

		tagName: 'li'

		, template: ItemTpl
		
	});

	return ItemView;
});