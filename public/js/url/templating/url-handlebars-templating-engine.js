define([
	'handlebars',
	'./url-abstract-templating-engine',
	// Ensure Object.create is cross browser compatible
	// 'es5-sham'
],
function(
	Handlebars,
	AbstractURLTemplatingEngine
) {

	/*
		URLHandlebarsTemplatingEngine class.
		URLHandlebarsTemplatingEngine implements AbstractURLTemplatingEngine interface.
	*/
	var URLHandlebarsTemplatingEngine = Object.create(AbstractURLTemplatingEngine);

	/*
		Implementation of abstract "render" function
		@see url/templating/url-templating-engine render function
	*/
	URLHandlebarsTemplatingEngine.render = function(template, context) {
		return Handlebars.compile(template)(context);
	};
	

	return URLHandlebarsTemplatingEngine;
});