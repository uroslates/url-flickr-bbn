define([
	// NOTE: specific templating engine implementation's module dependency
	'url/templating/url-handlebars-templating-engine'
],
function(
	TemplatingEngine
) {

	/*
		URL js templating engine wrapper.
		The purpose of this module is to encapsulate selected client-side templating engine.
		That way changing templating engine won't affect other files cause all files that
		are using rendering engine will still call a valid interface encapsulated by this module.

		To change the templating engine the only thing to be done is:
			# Create new templating engine implementation module containing specific templating
		engine implementation (class that implements AbstractURLTemplatingEngine interface).
		For example @see url-handlebars-templating-engine.js
			# Replace this module's dependency path to that new module path.

		After doing that new templating engine solution will be used within the application.

		NOTE: You still have to update all templates to use new templating solution syntax.

		
		Example usage:

			define([
				'jquery',
				'templatingEngine',
				'text!templates/test.html'
			],
			function ($, TemplatingEngine, TestTmpl) {

				var context = {
						data: 'URL > Handlebars'
					},
					html = TemplatingEngine.render(TestTmpl, context);

				$('body').html(html);

			});

	*/
	var URLTemplatingEngine = {
		
		/*
			Function that does rendering of specified template by using provided context.
			@argument template - html template to be rendered
			@argument context - context to be used when rendering template
		*/
		render: function(template, context) {
			return TemplatingEngine.render(template, context);
		}

	};

	return URLTemplatingEngine;
});