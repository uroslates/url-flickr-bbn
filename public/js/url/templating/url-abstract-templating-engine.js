define([],
function() {

	/*
		Abstract URL TemplatingEngine class.
		This class defines interface that has to be implemented for URL templating to work.
	*/
	var AbstractURLTemplatingEngine = {

		/*
			Renders template with provided context data.
			@argument template - html template to be rendered
			@argument context - context to be used when rendering template
			@returns rendered html (with context data utilized)
		*/
		render: function(template, context) {
			throw new Error('URLTemplatingEngine.render(template, context) not implemented!');
		}

	};


	return AbstractURLTemplatingEngine;
});