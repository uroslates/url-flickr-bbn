# Flickr API demo application

This is a small demo application which is consuming Flickr public API service and performs basic tag based search of images and their rendering.


## Requirements

* Create an app to consume a [Public JSON API Feed](http://api.flickr.com/services/feeds/photos_public.gne?tags=potato&tagmode=all&format=json&jsoncallback=?) ([documentation](http://www.flickr.com/services/feeds/docs/photos_public/)) and display the resulting data according to the  wireframes (Wireframes-spec.pdf).
* Site should function as a one-page app
* Layout should match the provided wireframes
* Look and feel is up to you, flat colours for borders and background are all that’s expected
* Supports the Google Apps Supported Browsers List
* Site should use responsive techniques to ensure it works on a range of devices
* Must be built using HTML, CSS and JS (SCSS or Less are encouraged)
* We strongly encourage the use of either AngularJS or Backbone for your module/application.
* Use any 3rd party libs you like (as long as they have a weak copyleft license)
* Search items e.g. a free-text search box where text entered is matched against tags
* Build process for generating production-ready code
* Tests (unit tests, e2e tests etc.)


## Instructions

Install dependencies:

	$ npm install
	$ bower install

Starting the app:

	$ npm start

Development process:

	$ grunt watch

Build for production:

	$ grunt build
Then change paths within views/layout.jade to match file paths with the production generated files (comment/uncomment lines).

Tests:

	$ npm test


## Technologies

* Node - server side
* Bower - frontend dependency management
 * Require - depenency management
 * Backbone.js - MVC like style
* Grunt - build
* Flickr Public API
* Compass - preprocessor
* Karma - testing


## Author

[Uros Lates](http://uroslates.com)